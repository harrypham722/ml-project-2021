import os

label_path = "./data/labels"
image_path = "./data/images"
csvfile_path = "./format_data.csv"

with open(csvfile_path, 'w') as csvfile:
    for (dirname, dirs, files) in os.walk(label_path):
        for filename in files: 
            with open(label_path + "/" + filename, "r") as f:
                line = f.read()
                arrInfo = line.split(' ')
                classNumber = arrInfo[0]
                x_center = float(arrInfo[1])
                y_center = float(arrInfo[2])
                weight = float(arrInfo[3])
                height = float(arrInfo[4])
                x_min = x_center - weight/2
                y_min = y_center - height/2
                x_max = x_center + weight/2
                y_max = y_center + height/2
                
                csvfile.write(
                    "{}/{}.jpg,{},{},{},{},{}\n".format(image_path, filename.split('.txt')[0],
                    x_min, y_min, x_max, y_max, classNumber
                    ))